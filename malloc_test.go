package cachepool

import (
	"fmt"
	"reflect"
	"runtime"
	"testing"
	"unsafe"
)

func TestMalloc(t *testing.T) {
	b := RawMallocByteSlice(128)
	for k := range b {
		b[k] = byte(k)
	}
	fmt.Println(b)
	runtime.GC()

	b = RawMallocByteSlice(128)
	fmt.Printf("---%s---\n", b)
	runtime.GC()

	b = RawByteSlice(128)
	fmt.Println(b)
	runtime.GC()

	b = RawByteSlice(127)
	bH := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	fmt.Printf("%#v\n", bH)
}
